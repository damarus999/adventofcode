import Solutions.Y2015.Day7;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class Day7Tests {

    @Test
    public void directSignalisParsedCorrectly(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "123");

        //Act
        int result = Day7.getValue("a", circuit);

        //Assert
        assertEquals(123, result);
    }

    @Test
    public void signalCorrectlyPassedAlongMultipleWires(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "123");
        circuit.put("b","a");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(123, result);
    }


    @Test
    public void notGateAcceptsDirectValues(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "NOT 0");

        //Act
        int result = Day7.getValue("a", circuit);

        //Assert
        assertEquals(65535, result);
    }

    @Test
    public void notGateCorrectlyNegatesIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","NOT a");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(65534, result);
    }

    @Test
    public void notGateReturns16BitIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "0");
        circuit.put("b","NOT a");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(65535, result);
    }

    @Test
    public void rShiftAcceptsDirectValues(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a","1 RSHIFT 1");

        //Act
        int result = Day7.getValue("a", circuit);

        //Assert
        assertEquals(0, result);
    }

    @Test
    public void rShiftCorrectlyShiftsIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "2");
        circuit.put("b","a RSHIFT 1");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(1, result);
    }

    @Test
    public void rShiftDropsLeastSignificantBit(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","a RSHIFT 1");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(0, result);
    }

    @Test
    public void lShiftAcceptsDirectValues(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a","1 LSHIFT 1");

        //Act
        int result = Day7.getValue("a", circuit);

        //Assert
        assertEquals(2, result);
    }

    @Test
    public void lShiftCorrectlyShiftsIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","a LSHIFT 1");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(2, result);
    }

    @Test
    public void lShiftDropsMostSignificantBit(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "32768");
        circuit.put("b","a LSHIFT 1");

        //Act
        int result = Day7.getValue("b", circuit);

        //Assert
        assertEquals(0, result);
    }

    @Test
    public void orGateAcceptsDirectInput(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a","0 OR 1");

        //Act
        int result = Day7.getValue("a", circuit);

        //Assert
        assertEquals(1, result);
    }

    @Test
    public void orGateCorrectlyOrsIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","3");
        circuit.put("c","a OR b");

        //Act
        int result = Day7.getValue("c", circuit);

        //Assert
        assertEquals(3, result);
    }

    @Test
    public void orGateReturns16BitIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","65536");
        circuit.put("c","a OR b");

        //Act
        int result = Day7.getValue("c", circuit);

        //Assert
        assertEquals(1, result);
    }

    @Test
    public void andGateAcceptsDirectInput(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a","1 AND 1");

        //Act
        int result = Day7.getValue("a", circuit);

        //Assert
        assertEquals(1, result);
    }

    @Test
    public void andGateCorrectlyAndsIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","3");
        circuit.put("c","a AND b");

        //Act
        int result = Day7.getValue("c", circuit);

        //Assert
        assertEquals(1, result);
    }

    @Test
    public void andGateReturns16BitIntegers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","65536");
        circuit.put("c","a AND b");

        //Act
        int result = Day7.getValue("c", circuit);

        //Assert
        assertEquals(0, result);
    }

    @Test
    public void wiresCanBeInputToSeveralOthers(){
        //Arrange
        HashMap<String,String> circuit = new HashMap<>();
        circuit.put("a", "1");
        circuit.put("b","a");
        circuit.put("c","a RSHIFT 5");

        //Act
        int bValue = Day7.getValue("b", circuit);
        int cValue = Day7.getValue("c", circuit);

        //Assert
        assertEquals(1, bValue);
        assertEquals(0, cValue);
    }
}

