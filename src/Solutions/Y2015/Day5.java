package Solutions.Y2015;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day5 {
    public static void main(String[] args) {
        //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF.
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);
        int niceStringCount = 0;
        int niceStringCountPartTwo = 0;

        while(scanner.hasNext()){
            String stringToCheck = scanner.next();
            if(isNiceStringPartOne(stringToCheck))
                niceStringCount++;

            if(isNiceStringPartTwo(stringToCheck))
                niceStringCountPartTwo++;
        }
        System.out.println("Number of nice strings according to Part 1 rules is: " + niceStringCount);
        System.out.println("Number of nice strings according to Part 2 rules is: " + niceStringCountPartTwo);
    }

    private static boolean stringContainsAtLeastThreeVowels(String string){
        int vowelCount = 0;
        for (int i = 0; i < string.length(); i++) {
            switch(string.charAt(i)){
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u' : vowelCount++;
                    break;
            }
        }
        return vowelCount > 2;
    }

    private static boolean stringContainsLetterThatAppearsTwiceInARow(String string){
        char previousCharacter = '-';
        char currentCharacter;
        for (int i = 1; i < string.length(); i++) {
            if (string.charAt(i) == string.charAt(i - 1))
                return true;
        }
        return false;
    }

    private static boolean stringContainsIllegalSequence(String string){
        return (string.contains("ab") || string.contains("cd") || string.contains("pq") || string.contains("xy"));
    }

    private static boolean stringContainsPairThatAppearsAtLeastTwiceWithNoOverlap(String string){
        Pattern pattern = Pattern.compile("(\\w\\w)\\w*\\1");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    private static boolean stringRepeatsWithExactlyOneCharBetween(String string){
        Pattern pattern = Pattern.compile("(\\w)\\w\\1");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    private static boolean isNiceStringPartOne(String stringToCheck){
        return stringContainsAtLeastThreeVowels(stringToCheck) &&
                stringContainsLetterThatAppearsTwiceInARow(stringToCheck) &&
                !stringContainsIllegalSequence(stringToCheck);
    }

    private static boolean isNiceStringPartTwo(String stringToCheck){
        return stringContainsPairThatAppearsAtLeastTwiceWithNoOverlap(stringToCheck) &&
                stringRepeatsWithExactlyOneCharBetween(stringToCheck);
    }
}
