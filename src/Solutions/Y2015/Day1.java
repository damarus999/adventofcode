package Solutions.Y2015;

import java.util.Scanner;

public class Day1 {

    public static void main(String[] args){
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        int floor = 0;
        int enteredBasementAt = -1;
        for (int i = 0; i < input.length(); i++) {
            switch(input.charAt(i)){
                case '(' : floor += 1;
                    break;
                default : floor -= 1;
                    break;
            }
            if(enteredBasementAt == -1 && floor == -1){
                enteredBasementAt = i + 1;
            }
        }
        System.out.println("Santa is on Floor: " + floor + " and first entered the basement at character: " + enteredBasementAt);
    }
}
