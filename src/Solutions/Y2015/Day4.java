package Solutions.Y2015;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Day4 {

    public static void main(String[] args) {
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        System.out.println("The lowest number that produces a hash with 5 leading zeroes is: " +
                getCounterThatGeneratesHashWithLeadingZeroes(5, input) +" and with 6 " +
                "leading zeroes is: " + getCounterThatGeneratesHashWithLeadingZeroes(6, input));
    }

    private static int getCounterThatGeneratesHashWithLeadingZeroes(int numberOfLeadingZeroes, String key){

        try {
            int counter = 0;
            byte[] byteData;
            StringBuffer sb;
            MessageDigest md = MessageDigest.getInstance("MD5");

            while (true) {
                byteData = md.digest((key + counter).getBytes());
                sb = new StringBuffer();
                for (int i = 0; i < byteData.length; i++) {
                    sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                }

                if(sb.toString().substring(0, numberOfLeadingZeroes)
                        .equals(String.format("%0"+(numberOfLeadingZeroes)+"d", 0))){
                    return counter;
                }
                counter++;
            }
        }
        catch(NoSuchAlgorithmException ex){
            return -1;
        }
    }
}
