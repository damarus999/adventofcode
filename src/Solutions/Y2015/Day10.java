package Solutions.Y2015;

import java.util.Scanner;

public class Day10 {

    public static void main(String[] args) {
        String input = "";
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);

        if (scanner.hasNext()) {
            input = scanner.next();
        }
        System.out.println("The length when the process is applied 40 times is " + lookAndSayLoop(input, 40).length());
        System.out.println("The length when the process is applied 50 times is " + lookAndSayLoop(input, 50).length());
    }

    private static String lookAndSay(String input) {
        StringBuilder sb = new StringBuilder();
        char[] inputChars = input.toCharArray();
        int numberCount = 1;
        for (int i = 0; i < inputChars.length; i++) {
            int currentNumber = inputChars[i];
            if ((i + 1) < inputChars.length && inputChars[i] == inputChars[i + 1]) {
                numberCount++;
            } else {
                sb.append(numberCount).append(inputChars[i]);
                numberCount = 1;
            }
        }
        return sb.toString();
    }

    private static String lookAndSayLoop(String input, int cycles) {
        String newInput = input;
        for (int i = 0; i < cycles; i++) {
            newInput = lookAndSay(newInput);
        }
        return newInput;
    }
}
