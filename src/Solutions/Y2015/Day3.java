package Solutions.Y2015;

import java.awt.*;
import java.util.HashMap;
import java.util.Scanner;

public class Day3 {

    public static void main(String[] args){
        System.out.print("Enter your input: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        HashMap<Point, Integer> santaLocationHistory = new HashMap<Point, Integer>();
        HashMap<Point, Integer> santaTeamLocationHistory = new HashMap<Point, Integer>();
        Point soloSantaCurrentLocation = new Point(0,0);
        Point santaCurrentLocation = new Point(0,0);
        Point roboSantaCurrentLocation = new Point(0,0);

        santaLocationHistory.put(new Point(santaCurrentLocation), 1);
        santaTeamLocationHistory.put(new Point(santaCurrentLocation), 1);

        for (int i = 0; i < input.length(); i++) {
            moveTeamSanta(i,santaCurrentLocation, roboSantaCurrentLocation,santaTeamLocationHistory, input.charAt(i));
            moveSanta(soloSantaCurrentLocation, santaLocationHistory, input.charAt(i));
        }
        System.out.println(("Santa delivered presents to " + santaLocationHistory.size()) + " houses " +
                "and Team Santa delivered presents to " + santaTeamLocationHistory.size() + " houses." );
    }

    private static void moveSanta(Point currentLocation, HashMap<Point,Integer> locationHistory, char movement){
        switch(movement){
            case '^' : currentLocation.move(currentLocation.x,currentLocation.y+1);
                locationHistory.put(new Point(currentLocation), locationHistory.getOrDefault(currentLocation, 0) + 1);
                break;
            case 'v' : currentLocation.move(currentLocation.x,currentLocation.y-1);
                locationHistory.put(new Point(currentLocation), locationHistory.getOrDefault(currentLocation, 0) + 1);
                break;
            case '>' : currentLocation.move(currentLocation.x+1,currentLocation.y);
                locationHistory.put(new Point(currentLocation),locationHistory.getOrDefault(currentLocation, 0) + 1);
                break;
            case '<' : currentLocation.move(currentLocation.x-1,currentLocation.y);
                locationHistory.put(new Point(currentLocation), locationHistory.getOrDefault(currentLocation, 0) + 1);
                break;
        }
    }

    private static void moveTeamSanta(int i, Point santaCurrentLocation, Point roboSantaCurrentLocation, HashMap<Point, Integer> locationHistory, char move){
        if(i % 2 == 0) {
            moveSanta(santaCurrentLocation, locationHistory, move);
        }
        else{
            moveSanta(roboSantaCurrentLocation, locationHistory, move);
        }
    }
}
