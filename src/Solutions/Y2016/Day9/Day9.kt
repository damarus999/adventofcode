package Solutions.Y2016.Day9

import java.util.regex.Pattern

fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val inputLine = readLine() ?: ""
    val part1Decompressed = createList(inputLine).map(::decompress).joinToString("")
    val part2Decompressed = ultimateDecompress(inputLine)

    println("Decompressed length is ${part1Decompressed.length}")
    println("Ultimate decompressed length is $part2Decompressed")
}

fun decompress(markerSequence: MarkerSequence): String {
    val decompressedSequence = StringBuilder()
    decompressedSequence.append(markerSequence.uncompressedData)
    if (markerSequence.dataToRepeat != "") {
        (1..markerSequence.numberOfRepeats)
                .forEach {
                    decompressedSequence.append(markerSequence.dataToRepeat)
                }
    }
    return decompressedSequence.toString()
}

fun ultimateDecompress(input: String): Long {
    if (!input.contains('('))
        return input.length.toLong()

    return createList(input)
            .parallelStream()
            .map(::decompress)
            .mapToLong(::ultimateDecompress)
            .sum()
}

private fun createList(input: String): MutableList<MarkerSequence> {
    var stringToProcess = input
    val markerSequences = mutableListOf<MarkerSequence>()
    val markerPattern: Pattern = Pattern.compile("\\((\\d+)x(\\d+)\\)")
    var matcher = markerPattern.matcher(stringToProcess)
    while (matcher.find()) {
        val uncompressedData = stringToProcess.substring(0 until matcher.start())
        val numberOfCharsToRepeat = matcher.group(1).toInt()
        val numberOfRepeats = matcher.group(2).toInt()
        val finalChar = matcher.end() + numberOfCharsToRepeat
        markerSequences.add(MarkerSequence(uncompressedData, stringToProcess.substring(matcher.end() until finalChar), numberOfRepeats))
        stringToProcess = stringToProcess.substring(finalChar)
        matcher = markerPattern.matcher(stringToProcess)
    }
    markerSequences.add(MarkerSequence(uncompressedData = stringToProcess))
    return markerSequences
}

data class MarkerSequence(val uncompressedData: String = "", val dataToRepeat: String = "", val numberOfRepeats: Int = 1)
