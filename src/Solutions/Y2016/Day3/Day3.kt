package Solutions.Y2016.Day3

import java.util.*
import java.util.regex.Pattern


fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    var triangleCounter = 0
    var part2TriangleCounter = 0
    var rowCounter = 1
    var side1List = IntArray(3)
    var side2List = IntArray(3)
    var side3List = IntArray(3)

    while (scanner.hasNext()) {
        val potentialTriangle = scanner.next()
        val pattern = Pattern.compile("(\\d+)\\s+(\\d+)\\s+(\\d+)")
        val matcher = pattern.matcher(potentialTriangle)
        matcher.find()
        val side1 = matcher.group(1).toInt()
        val side2 = matcher.group(2).toInt()
        val side3 = matcher.group(3).toInt()
        if (isValidTriangle(side1, side2, side3)) {
            triangleCounter++
        }
        when (rowCounter % 3) {
            0 -> {
                side3List = intArrayOf(side1, side2, side3)
                (0..2)
                        .filter { isValidTriangle(side1List[it], side2List[it], side3List[it]) }
                        .forEach { part2TriangleCounter++ }
            }
            1 -> {
                side1List = intArrayOf(side1, side2, side3)
            }
            2 -> {
                side2List = intArrayOf(side1, side2, side3)
            }
        }
        rowCounter++
    }
    println("The number of possible triangles for Part 1 is: $triangleCounter")
    println("The number of possible triangles for Part 2 is: $part2TriangleCounter")
}

private fun isValidTriangle(side1: Int, side2: Int, side3: Int): Boolean {
    return (side1 + side2 > side3
            && side1 + side3 > side2
            && side2 + side3 > side1)
}