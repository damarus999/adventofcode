package Solutions.Y2016.Day11

import java.util.*
import java.util.regex.Pattern

typealias FacilityState = MutableMap<Int, Set<Item>>

fun main(args: Array<String>) {
    val graph = mutableSetOf<Node>()
    val initialFacilityState = mutableMapOf<Int, Set<Item>>()
    val allItems = mutableSetOf<Item>()
    val goalState = mutableMapOf<Int, Set<Item>>()
    var currentFloor = 0
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")

    while (scanner.hasNext()) {
        currentFloor++
        populateFloor(currentFloor, scanner.next(), initialFacilityState)
    }

    initialFacilityState.forEach { (floor, floorItems) ->
        allItems.addAll(floorItems)
        goalState.put(floor, if (floor == initialFacilityState.size) allItems else setOf())
    }

    //from a node, generate all possible successor nodes
    var goalNode: Node? = null
    val startNode = Node(parentNode = null, facilityState = initialFacilityState, liftAtFloor = 1, expanded = false, costSoFar = 0, estimatedCostToGoal = Int.MAX_VALUE, actionTaken = Pair(0, listOf()))
    graph.add(startNode)

    var currentNode = startNode
    while (goalNode == null) {
        currentNode = getNextNode(currentNode, graph)
        var canGoInDirectionSafely = mutableListOf<List<Item>>()
        val itemsOnCurrentFloor = currentNode.facilityState.getValue(currentNode.liftAtFloor)
        for (direction in listOf(-1, 1)) {
            val newFloor = currentNode.liftAtFloor + direction
            if (newFloor > 0 && newFloor <= currentNode.facilityState.size) {
                generateCombinations(itemsOnCurrentFloor.toList())
                        .filterTo(canGoInDirectionSafely) { isSafeOnFloor(currentNode.facilityState.getValue(newFloor), it) }
                //If we can move 2 up, don't bother moving singles up
                //If we can move 1 down, don't bother moving pairs
                if (direction == 1) {
                    canGoInDirectionSafely = if (canGoInDirectionSafely.any { it.size > 1 })
                        canGoInDirectionSafely.filter { it.size > 1 }.toMutableList()
                    else canGoInDirectionSafely
                } else {
                    canGoInDirectionSafely = if (canGoInDirectionSafely.any { it.size == 1 })
                        canGoInDirectionSafely.filter { it.size == 1 }.toMutableList()
                    else canGoInDirectionSafely
                }
                goalNode = addNewNodesToGraph(canGoInDirectionSafely, graph, currentNode, direction, goalState)
            }
        }
        currentNode.expanded = true
    }
    println("It will take ${goalNode.costSoFar} steps to reach the goal, graph size: ${graph.size}")
    val actions = listActionsToNode(goalNode).asReversed()
    actions.forEach(::println)
}

fun generateCombinations(itemList: List<Item>): List<List<Item>> {
    val combinations = mutableListOf<List<Item>>()
    for (i in 0 until itemList.size) {
        combinations.add(listOf(itemList[i]))
        (i + 1 until itemList.size)
                .mapTo(combinations) { listOf(itemList[i], itemList[it]) }
    }
    return combinations
}

fun addNewNodesToGraph(itemLists: List<List<Item>>, graph: MutableSet<Node>, currentNode: Node, direction: Int, goalState: FacilityState): Node? {
    fun statesAreEquivalent(node1: Node, node2: Node): Boolean {
        //This most definitely needs optimised...
        //They are equivalent if the same floors contain the same number of chip/gen pairs, and all other items have equal isGenerator property?
        return (node1.facilityState == node2.facilityState
                || node1.facilityState.map { (_, items) -> Pair(items.map(Item::isGenerator), items.groupBy(Item::element).size) } ==
                node2.facilityState.map { (_, items) -> Pair(items.map(Item::isGenerator), items.groupBy(Item::element).size) })
                && node1.liftAtFloor == node2.liftAtFloor
    }

    fun getEquivalentNodes(node: Node): List<Node> {
        return graph.filter { statesAreEquivalent(it, node) }
    }

    val newFloor = currentNode.liftAtFloor + direction
    for (itemList in itemLists) {
        val newFacilityState = currentNode.facilityState.toMutableMap()
        val newFloorLayout = newFacilityState.getValue(newFloor).toMutableSet()
        val resultingCurrentFloorLayout = newFacilityState.getValue(currentNode.liftAtFloor).toMutableSet()
        itemList.forEach { newFloorLayout.add(it); resultingCurrentFloorLayout.remove(it) }
        newFacilityState.put(newFloor, newFloorLayout)
        newFacilityState.put(currentNode.liftAtFloor, resultingCurrentFloorLayout)
        val newNode = Node(currentNode, newFacilityState, newFloor, false, currentNode.costSoFar + 1, getEstimatedCost(newFacilityState), Pair(direction, itemList))
        val equivalentNodes = getEquivalentNodes(newNode)
        if (equivalentNodes.isEmpty()) {
            graph.add(newNode)
            if (newFacilityState == goalState) {
                return newNode
            }
        } else {
            if (newNode.costSoFar < (equivalentNodes.minBy(Node::costSoFar))!!.costSoFar) {
                equivalentNodes.forEach { removeSubTree(graph, it) }
                graph.add(newNode)
            }
        }
    }
    return null
}

fun listActionsToNode(node: Node?): List<Pair<Int, List<Item>>> {
    val actionList = mutableListOf(node!!.actionTaken)
    if (node.parentNode == null)
        return actionList
    else
        actionList.addAll(listActionsToNode(node.parentNode))
    return actionList
}

fun removeSubTree(graph: MutableSet<Node>, node: Node) {
    graph.remove(node)
    graph.filter { it.parentNode == node }
            .forEach { removeSubTree(graph, it) }
}

fun getNextNode(node: Node?, graph: Set<Node>): Node {
    return graph.filter { !it.expanded && (it.costSoFar + it.estimatedCostToGoal) <= node!!.costSoFar + node.estimatedCostToGoal }.sortedBy { it.estimatedCostToGoal + it.costSoFar }.firstOrNull() ?:
            graph.filter { !it.expanded }.sortedBy { it.estimatedCostToGoal + it.costSoFar }.first()
}

fun getEstimatedCost(state: FacilityState): Int {
    return state.map { (floor, items) -> items.size * (state.size - floor) }.sum()
}

fun populateFloor(floor: Int, floorDescription: String, facilityState: FacilityState) {
    val splitFloorDesc = floorDescription.split("contains")
    val floorComponents = splitFloorDesc[1].split(Pattern.compile("(,|and)"))
    val componentPattern = Pattern.compile("an? (\\w+)(-compatible microchip| generator)")
    val floorItemList = mutableSetOf<Item>()
    for (component in floorComponents) {
        val matcher = componentPattern.matcher(component)
        if (matcher.find()) {
            val element = matcher.group(1)
            val isGenerator = matcher.group(2).contains("generator")
            floorItemList.add(Item(element, isGenerator))
        }
    }
    facilityState.put(floor, floorItemList)
}

fun isSafeOnFloor(floorContents: Set<Item>, items: List<Item>): Boolean {
    fun individualItemIsSafeToMove(item: Item): Boolean {
        return floorContents.any { it.element == item.element && item.isGenerator != it.isGenerator }
                || (floorContents.all(Item::isGenerator) && item.isGenerator)
                || (floorContents.none(Item::isGenerator) && !item.isGenerator)
    }

    fun isChipGeneratorCombo(items: List<Item>): Boolean {
        return items.groupBy(Item::element).size == 1 && items.size > 1 //assuming no duplicate items
    }

    fun itemsAreSafeTogether(items: List<Item>): Boolean {
        return items.all(Item::isGenerator) || items.none(Item::isGenerator) || isChipGeneratorCombo(items)
    }
    return itemsAreSafeTogether(items) &&
            if (isChipGeneratorCombo(items))
                individualItemIsSafeToMove(items.filter(Item::isGenerator).first())
            else items.all(::individualItemIsSafeToMove)
}

data class Item(val element: String, val isGenerator: Boolean)
data class Node(val parentNode: Node?, val facilityState: Map<Int, Set<Item>>, val liftAtFloor: Int, var expanded: Boolean, val costSoFar: Int, val estimatedCostToGoal: Int, val actionTaken: Pair<Int, List<Item>>)