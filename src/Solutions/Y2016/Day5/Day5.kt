package Solutions.Y2016.Day5

import java.security.MessageDigest

fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    println("Enter your input: ")
    val doorId = readLine() ?: ""

    var counter = 0
    val messageDigester = MessageDigest.getInstance("MD5")
    val password = StringBuilder()
    val passwordPart2 = CharArray(8)
    val assignedSet = hashSetOf<Int>()

    while (true) {
        val decryptedString = decrypt(counter, doorId, messageDigester)
        if (decryptedString.startsWith("00000")) {
            var position: Int
            if (password.length < 8) {
                password.append(decryptedString[5])
            }
            try {
                position = decryptedString[5].toString().toInt()
            } catch(nfe: NumberFormatException) {
                position = 8
            }
            if (position < 8) {
                if (!assignedSet.contains(position)) {
                    assignedSet.add(position)
                    passwordPart2[position] = decryptedString[6]
                }
            }
        }
        if (assignedSet.joinToString("") == "01234567") {
            break
        }
        counter++
    }

    println("Password is $password")
    println("Password for the next door is ${passwordPart2.joinToString("")}")
}

private fun decrypt(counter: Int, doorId: String, messageDigester: MessageDigest): String {
    val byteData = messageDigester.digest(("$doorId$counter".toByteArray()))
    val stringBuilder = StringBuilder()
    (0..byteData.size - 1)
            .map { byteData[it].toInt() }
            .forEach { stringBuilder.append(Integer.toString((it and 0xff) + 0x100, 16).substring(1)) }
    return stringBuilder.toString()
}
