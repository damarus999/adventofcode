package Solutions.Y2016.Day7

import java.util.*
import java.util.regex.Pattern


fun main(args: Array<String>) {
    //Press enter after pasting the input and then press ctrl-D on Linux or ctrl-Z on Windows to signify EOF
    print("Enter your input: ")
    val scanner = Scanner(System.`in`).useDelimiter("\n")
    val abbaPattern = Pattern.compile("\\w*(\\w)((?!\\1)\\w)\\2\\1\\w*")
    val abbaInHyperNetPattern = Pattern.compile("\\[\\w*(\\w)((?!\\1)\\w)\\2\\1\\w*\\]")
    val part2Pattern = Pattern.compile("((?!(\\w)\\2)(\\w)(\\w)\\3(?:\\[[^\\]]*\\]|\\][^\\[]*\\[|[^\\[\\]])*[\\[\\]][^\\[\\]]*\\4\\3\\4)")
    var tlsIps = 0
    var sslIps = 0

    while (scanner.hasNext()) {
        val ip = scanner.next()
        val abbaMatcher = abbaPattern.matcher(ip)
        val abbaInHyperNetMatcher = abbaInHyperNetPattern.matcher(ip)
        val part2Matcher = part2Pattern.matcher(ip)
        if (abbaMatcher.find() && !abbaInHyperNetMatcher.find()) {
            tlsIps++
        }
        if (part2Matcher.find()) {
            sslIps++
        }
    }
    println("The number of IPs supporting TLS is: $tlsIps")
    println("The number of IPs supporting SSL is $sslIps")
}